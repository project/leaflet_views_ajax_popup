(function ($) {

  $(document).bind('leaflet.map', function (e, settings, lMap) {
    lMap.on('popupopen', function (e) {
      var content = $('.leaflet-ajax-popup', e.popup._contentNode);

      if (content.length) {
        var url = Drupal.settings.basePath + Drupal.settings.pathPrefix + 'leaflet-views-ajax-popup';
        url += '/' + content.data('type');
        url += '/' + content.data('id');
        url += '/' + content.data('mode');
        $.get(url, function (response) {
          if (response) {
            e.popup.setContent(response);
          }
        });
      }
    });
  });

})(jQuery);
